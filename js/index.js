showfav();
let elems = document.getElementsByClassName("fa-heart");
for (i = 0; i < elems.length; i++) {
  elems[i].addEventListener("click", function () {
    let moviename =
      this.parentNode.parentElement.parentElement.children[1].childNodes[1]
        .children[0].innerText;
    let moviesrc = this.parentNode.parentElement.lastElementChild.src;
    let movietag =
      this.parentNode.parentElement.parentElement.children[1].childNodes[1]
        .children[1].innerText;
    let movietext =
      this.parentNode.parentElement.parentElement.children[1].childNodes[1]
        .children[2].innerText;
    let moviedir =
      this.parentNode.parentElement.parentElement.children[1].childNodes[1]
        .children[3].innerText;
    let movieinfo =
      this.parentNode.parentElement.parentElement.children[1].childNodes[1]
        .children[4].innerText;
    let itemobj;
    let items = localStorage.getItem("items");
    if (!items) {
      itemobj = [];
    } else {
      itemobj = JSON.parse(items);
    }
    var flag = 0;
    if (localStorage.getItem('items')) {
      for (i = 0; i < JSON.parse(localStorage.getItem("items")).length; i++) {
        if (JSON.parse(localStorage.getItem("items"))[i] == moviename) {
          flag = 1;
          break;
        }
      }
    }
    if (flag == 0) {
      itemobj.push(moviename);
      itemobj.push(moviesrc);
      itemobj.push(movietag);
      itemobj.push(movietext);
      itemobj.push(moviedir);
      itemobj.push(movieinfo);
    }
    localStorage.setItem("items", JSON.stringify(itemobj));
    showfav();
  });
}
function showfav() {
  let items = localStorage.getItem("items");
  let itemobj;
  if (!items) {
    itemobj = [];
  } else {
    itemobj = JSON.parse(items);
    let html = "";
    for (i = 0; i < itemobj.length; i = i + 6) {
      html += `<div class="card mb-3" style="max-width: 100vw;">
      <div class="row g-0">
         <div class="col-md-4 padit" style="margin: auto;display: flex;">
            <div class="symbol"><i class="fas fa-trash" id=${i} onclick="deletefav(this.id)"></i></div>
            <img src="${itemobj[i + 1]}" style="margin: auto;"
               class="img-fluid rounded-start retrieveimg" alt="Jai Bhim">
         </div>
         <div class="col-md-8">
            <div class="card-body">
               <h4 class="card-title retrievetitle" style="font-family: 'Poppins', sans-serif;">${
                 itemobj[i]
               }</h4>
               <h5 class="card-title retrievetag" style="font-family: 'Poppins', sans-serif;">${
                 itemobj[i + 2]
               }
               </h5>
               <div class="card-text retrievetext" style="font-family: 'Roboto', sans-serif;">${
                 itemobj[i + 3]
               }</div>
               <div class="card-text" style="font-family: 'Roboto', sans-serif;"><small
                     class="text-muted retrievedir">${
                       itemobj[i + 4]
                     }</small></div>
               <div class="card-text" style="font-family: 'Roboto', sans-serif;">
                  <small class="text-muted retrieveinfo">
                     <p>${itemobj[i + 5]}
                  </small>
                  </p>
               </div>
            </div>
         </div>
      </div></div>`;
    }
    let elem = document.getElementsByClassName("showhere")[0];
    elem.innerHTML = html;
  }
}
function deletefav(id){
   let items=localStorage.getItem('items')
   let itemobj;
   if(items==null){
      itemobj=[]
   }
   else{
      itemobj=JSON.parse(items)
   }
   console.log(itemobj)
   itemobj.splice(id,6)
   console.log(itemobj)
   localStorage.setItem('items',JSON.stringify(itemobj))
   showfav()
}